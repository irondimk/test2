import React, {useEffect, useState} from 'react';
import User from './User';
const Users = () => {

    let [users, setUsers] = useState([]);

    useEffect(()=> {
        fetch('https://jsonplaceholder.typicode.com/users').then((response)=> {
            return response.json()
        }).then((json)=> {
            setUsers(json);
        })
    }, [])

    return (
        <div>
            {users.map((item)=> {
                return <User key={item.name} name={item.name} phone={item.phone} website={item.website}/>
            })}
        </div>
    );
};

export default Users;